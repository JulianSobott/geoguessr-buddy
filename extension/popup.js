async function btnMyGamesClick(e) {
    console.log("Clicked")
    // send message "readContent" to current tab with callback function
    let backgroundWindow  = await browser.runtime.getBackgroundPage();
    backgroundWindow.goToMyGames();

}

const btnMyGames = document.getElementById("btnMyGames")

btnMyGames.addEventListener("click", e => {
    btnMyGamesClick(e).then(r => {
        console.log(r);
    })
});