function goToMyGames() {
    browser.tabs.query({active: true, currentWindow: true}, tabs => {
        browser.tabs.sendMessage(tabs[0].id, {action: "getPlayerId"}).then(response => {
            const playerId = response.playerId;
            const url = `https://geoguessr-buddy.lol-stats.de/player/${playerId}`;
            console.log(url)
            browser.tabs.create({url: url, active: true}).then(tab => {
                console.log(tab);
            });
        })
            .catch(error => {
                console.error(`Error: ${error}`);
            })
    });
}
