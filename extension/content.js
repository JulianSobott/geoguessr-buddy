function injectScript() {
    const script = document.createElement('script');
    script.src = browser.runtime.getURL('getGame.js');
    script.onload = function() {
        this.remove();
    };
    (document.head || document.documentElement).appendChild(script);
}

injectScript();

window.addEventListener('message', function(event) {
    // Only accept messages from the current page
    if (event.source !== window) return;

    if (event.data && event.data.action === 'sendGameData') {
        console.log(event.data);
    }
});

browser.runtime.onMessage.addListener(request => {
    if (request.action === "readContent") {
        console.log("readContent")
        document.dispatchEvent(new Event('getGameEvent'));
    }
    if(request.action === "getPlayerId") {
        const gameDataContainer = document.getElementById("__NEXT_DATA__");
        const gameData = JSON.parse(gameDataContainer.innerHTML);
        const playerId = gameData.props.accountProps.account.user.userId;
        return Promise.resolve({playerId});
    }
});

//
function addExportButtonIfDivExists() {
    let targetDiv = document.querySelector('[class^="game-finished_actions"]');
    if (targetDiv) {
        if (document.getElementById('exportGameData')) {
            return;
        }
        // const style = "display: flex; flex-direction: column; align-items: center; justify-content: center; width: 100%; height: 100%;";
        targetDiv.style["display"] = "flex";
        targetDiv.style["flex-direction"] = "row";
        targetDiv.style["align-items"] = "center";
        targetDiv.style["justify-content"] = "space-evenly";
        targetDiv.style["max-width"] = "50rem";

        const buttons = targetDiv.querySelector('[class^="buttons_buttons"]')
        const button = buttons.children[0].cloneNode(true);
        const span = button.querySelector('[class^="button_label"]');
        span.id = 'exportGameData';
        span.innerText = 'Continue and Save';
        targetDiv.appendChild(button);

        button.addEventListener('click', function(e) {
            e.preventDefault();
            e.stopPropagation();
            document.dispatchEvent(new Event('getGameEvent'));

            // listen for message
            document.addEventListener('gameDataSent', function() {
                console.log("gameDataSent");
                buttons.children[0].click();
            });

            document.addEventListener('gameDataError', function(e) {
                console.log("gameDataError");
                const errorContainer = document.createElement('div');
                errorContainer.innerText = e.detail.error;
                errorContainer.style["color"] = "red";
                targetDiv.appendChild(errorContainer);
            });
        });

        // Optionally, disconnect the observer after adding the button
        observer.disconnect();
    }
}

let observer = new MutationObserver(mutations => {
    for (let mutation of mutations) {
        if (mutation.type === 'childList') {
            addExportButtonIfDivExists();
        }
    }
});

observer.observe(document.body, { childList: true, subtree: true });

addExportButtonIfDivExists();
