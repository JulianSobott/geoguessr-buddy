const attributesString = "__reactFiber$lh7gqgw93re.return.return.return.memoizedState.next.next.next.next.next.next.next.next.next.next.next.next.next.next.next.next.next.next.next.next.next.next.next.next.next.next.memoizedState"

console.log("Loaded injected script")
let playerId = null;
let gameId = null;
let gameRound = null;

function get_state() {
    const root=  document.querySelector('[class^="game-finished_container"]');
    const attributes = attributesString.split(".");
    return getObject(root, attributes, ["__reactFiber"]);
}


function getObject(parent, attributes, prefixes) {
    let object = parent;
    for (let i = 0; i < attributes.length; i++) {
        // if attribute starts with __reactFiber then find the attribute that starts with __reactFiber
        try {
            let found = false;
            for (let prefix of prefixes) {
                if (attributes[i].startsWith(prefix)) {
                    for (let key in object) {
                        if (key.startsWith(prefix)) {
                            object = object[key];
                            found = true;
                            break;
                        }
                    }
                }
            }
            if (!found) {
                object = object[attributes[i]];
            }
        } catch (e) {
            // log attributes[i] and error
            return {error: e, attribute: attributes[i], i};
        }
    }
    return object;
}

function getLobby() {
    // json object from #__NEXT_DATA__ script tag
    const nextData = document.getElementById("__NEXT_DATA__");
    const nextDataJson = JSON.parse(nextData.innerHTML);
    const lobby = nextDataJson.props.pageProps.lobbyToJoin;
    if (!lobby) {
        console.log("no lobby found");
    }
    return lobby
}

function sendGameData() {
    const state = get_state();
    const lobby = getLobby();
    if (state) {
        const data = {
            state,
            lobby
        };
        fetch('https://geoguessr-buddy.lol-stats.de/game', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then(r => {
            if (r.status !== 201 && r.status !== 200) {
                r.text().then(bodyText => {
                    throw new Error(r.statusText + " " + bodyText);
                }).catch(e => {
                    console.log(e);
                    document.dispatchEvent(new CustomEvent('gameDataError', {detail: {error: e}}));
                });
            } else {
                document.dispatchEvent(new Event('gameDataSent'));
            }
        }).catch(e => {
            console.log(e);
            document.dispatchEvent(new CustomEvent('gameDataError', {detail: {error: e}}));
        });
    }
}

document.addEventListener('getGameEvent', function() {
    console.log("getGameEvent");
    sendGameData();
});


function updateCurrentRound() {
    // get contaioner: parent has attribute data-qa="round-number". container itself has class=^"status_value"
    const parent = document.querySelector('[data-qa^="round-number"]');
    if (!parent) {
        return;
    }
    const container = parent.querySelector('[class^="status_value"]');
    console.log(container)
    if (container) {
        const round = parseInt(container.innerText.split("/")[0].trim());
        if (round !== gameRound) {
            gameRound = round;
            document.dispatchEvent(new Event('roundChanged'));
            console.log("roundChanged")
        }
        // TODO: move this to a new method that is called, when game starts
        gameId = window.location.href.split("/").pop()
    }
}

let closure = null;

const gameDataContainer = document.getElementById("__NEXT_DATA__");
const gameData = JSON.parse(gameDataContainer.innerHTML);
playerId = gameData.props.accountProps.account.user.userId;
updateCurrentRound();

function loadClosure() {
    closure = getObject(window, "window.0.parent.1.frameElement.attributes.0.ownerDocument.closure_lm_662401".split("."), ["closure_lm"])
    console.log(closure)
}

// track path of player:
// lat long: window[0].parent[1].frameElement.attributes[0].ownerDocument.closure_lm_662401.B.visibilitychange[1].Fa.lc.B.RenderComplete[0].Fa.B.B.C.C.Y.D[0]
// listen for changes to this object
// on mousle click, get lat long and add to array

let lastLatLong = null;

document.addEventListener('click', function(e) {
    updateCurrentRound();
    captureTrackingData();
});

// document.addEventListener('roundChanged', function() {
//     // send new round to server
//     console.log("roundChanged in listener")
//     if (gameRound === 1) {
//         fetch("https://geoguessr-buddy.lol-stats.de/ingame/tracking/new", {
//             method: 'POST',
//             headers: {
//                 'Content-Type': 'application/json',
//             },
//             body: JSON.stringify({player_id: playerId, game_id: gameId}),
//         }).then(r => {
//             console.log(r)
//         }).catch(e => {
//             console.log(e);
//         });
//     }
// });

function captureTrackingData() {
    const data = getLatLong();
    let isSame = false;
    if (lastLatLong) {
        isSame = data.movement.location.lat === lastLatLong.lat && data.movement.location.lng === lastLatLong.lng;
    }
    if (!isSame) {
        sendTrackingData(data);
        lastLatLong = data.movement.location;
    }
}

function getLatLong() {
    if (!closure) {
        loadClosure();
    }
    const latLong = closure.B.visibilitychange[1].Fa.lc.B.RenderComplete[0].Fa.B.B.C.C.Y.D[0];
    console.log(latLong)
    return {
        player_id: playerId,
        game_id: gameId,
        round: gameRound,
        movement: {
            timestamp: 0,
            location: {
                lat: latLong[1],
                lng: latLong[2]
            }
        }
    };
}

function sendTrackingData(data) {
    console.log(data)
    fetch('https://geoguessr-buddy.lol-stats.de/ingame/tracking', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
    }).then(r => {
        if (r.status !== 201 && r.status !== 200) {
            r.text().then(bodyText => {
                throw new Error(r.statusText + " " + bodyText);
            }).catch(e => {
                console.log(e);
                document.dispatchEvent(new CustomEvent('gameDataError', {detail: {error: e}}));
            });
        } else {
            document.dispatchEvent(new Event('gameDataSent'));
        }
    }).catch(e => {
        console.log(e);
        document.dispatchEvent(new CustomEvent('gameDataError', {detail: {error: e}}));
    });
}