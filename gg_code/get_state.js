const attributesString = "__reactFiber$lh7gqgw93re.return.return.return.memoizedState.next.next.next.next.next.next.next.next.next.next.next.next.next.next.next.next.next.next.next.next.next.next.next.next.next.next.memoizedState"

function get_state() {
    const root=  document.querySelector('[class^="game-finished_container"]');
    const attributes = attributesString.split(".");
    return getObject(root, attributes);
}


function getObject(parent, attributes) {
    let object = parent;
    for (let i = 0; i < attributes.length; i++) {
        // if attribute starts with __reactFiber then find the attribute that starts with __reactFiber
        if (attributes[i].startsWith("__reactFiber")) {
            for (let key in object) {
                if (key.startsWith("__reactFiber")) {
                    object = object[key];
                    break;
                }
            }
        } else {
            object = object[attributes[i]];
        }
    }
    return object;
}