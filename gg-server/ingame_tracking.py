from fastapi import APIRouter, Request, Response, File
from pydantic import BaseModel

import db

router = APIRouter()


class Tracking(BaseModel):
    game_id: str
    player_id: str
    round: int
    movement: db.Movement


@router.post("/tracking")
async def save_tracking(tracking: Tracking):
    print(tracking)
    await db.add_move(tracking.player_id, tracking.game_id, tracking.round, tracking.movement)
    # await db.add_ingame_tracking(tracking.game_id, tracking.player_id, tracking.round, tracking.movement)


class NewTracking(BaseModel):
    game_id: str
    player_id: str


# @router.post("/tracking/new")
# async def new_tracking(data: NewTracking):
#     await db.new_ingame_tracking(data.player_id, data.game_id)
#     return Response("OK")
