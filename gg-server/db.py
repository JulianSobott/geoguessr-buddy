import os
from typing import Optional, Annotated, Coroutine
import asyncio

import motor.motor_asyncio
from bson import ObjectId
from pydantic import BaseModel, Field, BeforeValidator

client = motor.motor_asyncio.AsyncIOMotorClient(os.environ["MONGODB_URL"])
db = client.geoguessr_buddy
games_collection = db.games
players_collection = db.players
ingame_tracking_collection = db.ingame_tracking
player_movements_collection = db.player_movements

PyObjectId = Annotated[str, BeforeValidator(str)]


class Player(BaseModel):
    id: str = Field(alias="_id")
    name: str
    avatar: str
    games: list["Game"] = []


class PlayerCollection(BaseModel):
    players: list[Player]


class Game(BaseModel):
    id: str = Field(alias="_id")
    players: list[Player]
    mode: str
    map: str
    score: int
    datetime: int
    game_options: dict
    data: dict | None = None

    def model_dump_references(self):
        # exclude players.games to avoid circular reference
        data = self.model_dump(by_alias=True, exclude={"data", "players.games"})
        for player in data["players"]:
            player.pop("games")
        return data


class Location(BaseModel):
    lat: float
    lng: float


class Movement(BaseModel):
    timestamp: int
    location: Location


class Round(BaseModel):
    startTime: int
    endTime: int
    score: dict
    moves: list[Movement]


class InGameTracking(BaseModel):
    game_id: str
    player_id: str
    rounds: dict[str, Round]


class PlayerMovement(BaseModel):
    player_id: str
    game_id: str
    round: int
    moves: list[Movement]


async def add_game(game: Game):
    res1 = games_collection.insert_one(game.model_dump(by_alias=True))
    results = [res1]
    for player in game.players:
        res = players_collection.update_one(
            {"_id": player.id}, {"$push": {"games": game.model_dump_references()}}
        )
        results.append(res)
    return asyncio.gather(*results)


async def get_game(game_id: str) -> Game:
    return Game(**(await games_collection.find_one({"_id": game_id})))


async def upsert_player(player: Player):
    if not await does_player_exist(player.id):
        res = await players_collection.insert_one(player.model_dump(by_alias=True))
        return res
    res = await players_collection.update_one(
        {"_id": player.id}, {"$set": {"name": player.name, "avatar": player.avatar}}
    )
    return res


async def get_games_by_player(player_id: str) -> list[Game]:
    return (await players_collection.find_one({"_id": player_id}))["games"]


async def get_player(player_id: str) -> Player:
    return await players_collection.find_one({"_id": player_id})


async def does_player_exist(player_id: str) -> bool:
    return (await players_collection.count_documents({"_id": player_id})) > 0


async def does_game_exist(game_id: str) -> bool:
    return (await games_collection.count_documents({"_id": game_id})) > 0


async def add_move(player_id: str, game_id: str, game_round: int, movement: Movement):
    if not await player_movements_collection.find_one({"player_id": player_id, "game_id": game_id, "round": game_round}):
        await player_movements_collection.insert_one(
            {"player_id": player_id, "game_id": game_id, "round": game_round, "moves": []}
        )
    return await player_movements_collection.update_one(
        {"player_id": player_id, "game_id": game_id, "round": game_round},
        {"$push": {"moves": movement.model_dump(by_alias=True)}},
    )


async def get_moves(game_id: str, game_round: int) -> list[PlayerMovement]:
    # find all objects with game_id and round
    res = player_movements_collection.find({"game_id": game_id, "round": game_round})
    # convert to list
    res_list = await res.to_list(length=None)
    # convert to PlayerMovement objects
    return [PlayerMovement(**r) for r in res_list]


# async def new_ingame_tracking(player_id: str, game_id: str):
#     def empty_round():
#         return {"startTime": 0, "endTime": 0, "score": {}, "moves": []}
#     return await ingame_tracking_collection.insert_one(
#         {"player_id": player_id, "game_id": game_id, "rounds": {str(r): empty_round() for r in range(5)}}
#     )
#
#
# async def add_ingame_tracking(player_id: str, game_id: str, game_round: int, movement: Movement):
#     return await ingame_tracking_collection.update_one(
#         {"player_id": player_id, "game_id": game_id},
#         {"$push": {f"rounds.{game_round}.moves": movement.model_dump(by_alias=True)}},
#     )
#
#
# async def get_round(player_id: str, game_id: str, game_round: int) -> Round:
#     return InGameTracking(
#         **(await ingame_tracking_collection.find_one({"player_id": player_id, "game_id": game_id}))
#     ).rounds[str(game_round)]
