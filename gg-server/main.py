import asyncio
import os
import re

from fastapi import FastAPI, Response, File
from pydantic import BaseModel
from starlette.middleware.cors import CORSMiddleware
from starlette.requests import Request
from fastapi.templating import Jinja2Templates
from starlette.responses import FileResponse
from starlette.staticfiles import StaticFiles
import json
from datetime import datetime

import db
from ingame_tracking import router as ingame_router

app = FastAPI()

app.include_router(ingame_router, prefix="/ingame")

app.mount("/static", StaticFiles(directory="static"), name="static")
templates = Jinja2Templates(directory="templates")

origins = [
    "https://www.geoguessr.com",
]

icon_base_url = "https://www.geoguessr.com/images/resize:auto:48:48/gravity:ce/plain/"


def meters_to_kilometers(value):
    if value >= 1000:
        # round to 2 decimals
        return f"{value / 1000:.0f} km"
    else:
        return f"{value:.0f} m"


def format_datetime(value: int):
    return datetime.fromtimestamp(value / 1000).strftime("%Y-%m-%d %H:%M:%S")


def format_duration(value):
    # seconds to minutes and seconds (if no minutes then only seconds)
    if value >= 3600:
        hours = value // 3600
        minutes = (value % 3600) // 60
        seconds = value % 60
        return f"{hours:.0f} h {minutes:.0f} min {seconds:.0f} s"
    if value >= 60:
        minutes = value // 60
        seconds = value % 60
        return f"{minutes:.0f} min {seconds:.0f} s"
    else:
        return f"{value:.0f} s"


templates.env.filters["m_to_km"] = meters_to_kilometers
templates.env.filters["datetime"] = format_datetime
templates.env.filters["duration"] = format_duration


@app.get("/")
async def index(request: Request):
    return templates.TemplateResponse(request=request, name="index.html")


@app.get("/plugin/download/{version}.xpi")
async def download_plugin(version: str):
    if version != "latest" and not re.match(r"^\d+\.\d+\.\d+$", version):   # not (== latest or regex)
        return Response("Invalid version", status_code=400)
    path = f"static/plugin/geoguessr-buddy-{version}.xpi"
    if not os.path.exists(path):
        return Response("Version not found", status_code=404)
    return FileResponse(path, media_type="application/x-xpinstall")


@app.get("/player/{player_id}")
async def get_games(request: Request, player_id: str):
    if not await db.does_player_exist(player_id):
        return Response("Player not found", status_code=404)
    games = await db.get_games_by_player(player_id)
    player = await db.get_player(player_id)
    return templates.TemplateResponse(request=request, name="games.html", context={"games": games, "player": player})


@app.get("/game/{game_id}")
async def get_game(request: Request, game_id: str):
    if not await db.does_game_exist(game_id):
        return Response("Game not found", status_code=404)
    game = await db.get_game(game_id)
    raw_players = game.players
    players = [ p.model_dump(by_alias=True, exclude={"games"}) for p in raw_players]
    max_points = 0
    actual_points = 0
    total_time = 0
    can_calculate_total_time = "endTime" in game.data["rounds"][0]
    last_start_time = game.data["rounds"][0]["startTime"]
    for r in game.data["rounds"]:
        max_points += r["score"]["maxPoints"]
        actual_points += r["score"]["points"]
        if can_calculate_total_time:
            total_time += r["endTime"] - r["startTime"]
        else:
            total_time += r["startTime"] - last_start_time
            last_start_time = r["startTime"]
    game.data["total_time"] = total_time / 1000
    game.data["max_points"] = max_points
    game.data["actual_points"] = actual_points
    return templates.TemplateResponse(request=request, name="game.html", context={"game": game.data, "players": players})


@app.get("/api/game/{game_id}/round/{round_id}/movement")
async def get_round_movement(request: Request, game_id: str, round_id: int):
    if not await db.does_game_exist(game_id):
        return Response("Game not found", status_code=404)
    moves = await db.get_moves(game_id, round_id)
    return moves


@app.post("/game")
async def save_game(request: Request):
    data = await request.json()
    if await db.does_game_exist(data["state"]["gameId"]):
        return Response("Game already exists", status_code=400)
    state = data["state"]
    lobby = data["lobby"]
    players = lobby["players"]
    max_points = 0
    actual_points = 0
    for r in state["rounds"]:
        max_points += r["score"]["maxPoints"]
        actual_points += r["score"]["points"]
    state["max_points"] = max_points
    state["actual_points"] = actual_points
    db_players = [db.Player(_id=p["playerId"], name=p["nick"], avatar=icon_base_url + p["avatarPath"]) for p in players]
    game = db.Game(
        _id=state["gameId"],
        players=db_players,
        mode=lobby["gameType"],
        map=lobby["mapName"],
        score=state["actual_points"],
        datetime=state["rounds"][0]["startTime"],
        game_options=lobby["gameOptions"],
        data=state,
    )
    results = []
    for player in db_players:
        results.append(db.upsert_player(player))
    await asyncio.gather(*results)
    await db.add_game(game)


app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="127.0.0.1", port=8000)
